
## graphql-schema-utilities

From : https://github.com/awslabs/graphql-schema-utilities

#### Usage: 
 
``` 
 graphql-schema-utilities -s "{./root.graphql,./post/schema.graphql,./user/schema.graphql,./event/schema.graphql}" -o merged.graphql
```

